SECTION .bss

;los dos contadores están guardando la cantidad de digitos introducidos en ascii por el usuario
contadorx: resb 4
contadory: resb 4
;los dos numeros tienen los enteros que corresponden a los asciis introducidos por el usuario
numerox: resb 4
numeroy: resb 4
;en matriz se almacenan los asciis que corresponden a la matriz
;tamaño máximo de matriz 15x15
matriz: resb 225
;letra que corresponde al movimiento
letra_revisada: resb 4

;numero de argumentos que se guardan
n_argumentos_introducidos: resb 4

;guarda el --(cosa por hacer) insertada por el usuario como argumento
argumento_opcion: resb 4
largo_opcion: resb 4

nombre_archivo: resb 4


entrada_movimiento_tamanno: resb 4

;sirven para guardar largo de la entrada del usuario y la entrada respectivamente
largox: resb 4
largoy: resb 4
entrada_desplegable_x: resb 4
entrada_desplegable_y: resb 4

;posicion del jugador
desplegablex: resb 4
desplegabley: resb 4

;random
numero: resb 4
;posicion de bombas y tesoro
BombaX: resb 4
BombaY: resb 4
TesoroX: resb 4
TesoroY: resb 4

;usado en el manejo de archivos, donde se guarda el fd
id1: resb 4

;total de casillas de la matriz
total_matriz: resb 12
;para guardar el archivo de texto en caso de cargar un nuevo archivo
matriz_cargada:resb 250

posicionPX: resb 4
posicionPY: resb 4
SECTION .data
; Definición de los mensajes y sus respectivos largos
;los mensajes con unicamente espacios vacios servirán para ser modificados y guardar datos

mensaje: db "Ingrese numero de filas y columnas mayores para su tablero", 0x0A
largo: equ $-mensaje

mensaje1: db "Ingrese el número de columnas:",0x0A
largo1: equ $-mensaje1

;aqui se guarda el numero en ascii que corresponde a x
x: db "   ", 0x0A  
largo3: equ $-x

mensaje2: db "Ingrese el número de filas:",0x0A
largo4: equ $-mensaje2

;aqui se guarda el numero en ascii que corresponde a y
y: db "   ", 0x0A  
largo5: equ $-y

;usado para imprimir matriz
linea_horizontal: db " -----------------------------------------------------------------", 0x0A
largo_horizontal: equ $-linea_horizontal
;usado para imprimir matriz
linea_vertical: db " | "
largo_vertical: equ $-linea_vertical

salto_linea: db 0x0A
espacio: db " "

mensaje3: db "Espere mientras carga el juego", 0x0A
largo8: equ $-mensaje3

mensaje4: db "Ingrese la letra que desea introducir: ", 0x0A
mensaje5: db "(Sus opciones son w,a,s,d estas mueven hacia arriba, izquierda, abajo, derecha correspondientemente)(Puede guardar introduciendo el numero 1) ", 0x0A
largo9: equ $-mensaje4

entrada_movimiento: db " ", 0x0A
largo10: equ $-entrada_movimiento

entrada_usuario: db " ", 0x0A

mensaje_error: db "Ingresó un caracter no valido", 0x0A
largo_error: equ $-mensaje_error

mensaje_error2: db "Ingresó un 0, con ello es imposible crear una matriz", 0x0A, 0x0A
largo_error2: equ $-mensaje_error2

limite_matriz: db "El máximo de matriz que se puede ingresar es de 15x15", 0x0A
largo_limite: equ $-limite_matriz

mensaje_perdedor: db "Felicidades perdiste, una derrota es una lección para la victoria", 0x0A
largo_perdedor: equ $-mensaje_perdedor

mensaje_ganador: db "Felicidades ganaste, merecemos un 100 en el proyecto", 0x0A
largo_ganador: equ $-mensaje_ganador

mensaje_menu: db "Ingrese su opción", 0x0A
mensaje_jugar: db "1. Crear nuevo juego", 0x0A
mensaje_cargar: db "2. Cargar juego", 0x0A
largo_menu: equ $-mensaje_menu


archivo_matriz2: db "matrizcargada.txt",0h
archivo_matriz3: db "                 ",0h
;usado para separar datos en el archivo a guardar
separador: db "="
tiempos:                      ;utilizados para delay en el sistema.
    tv_sec  dd 0
    tv_usec dd 0

SECTION .text

global _start

_start:
;nop para que sea amigable con gdb
    nop
    
;guardamos el numero de parametros que se enviaron y le restamos uno para evitar contar la que llama al programa
    pop ecx
    dec ecx
    mov dword[n_argumentos_introducidos],ecx

;este selecciona la accion que se debe tomar
;en caso de tener 0 parametros corre el programa normal
;en el caso de 3 parametros los utilizar como x y y
;si son dos parametros carga el segundo como archivo de partida guardada
_verificar_inicio:
    cmp ecx, 0
    jz _menu
    cmp ecx, 3
    jz _iniciar_juego
    cmp ecx, 2
    jz _iniciar_cargar

;se saca un parametro que será el nombre del proyecto y se guardan los otros tres
;se asigna cada uno al espacio en memoria que les corresponde
_iniciar_juego:
    pop eax
    pop eax
    mov dword[argumento_opcion], eax
    call _sacar_largo
    mov dword[largo_opcion],edx
    pop eax
    mov dword[x], eax
    call _sacar_largo
    mov dword[contadorx],edx
    pop eax
    mov dword[y], eax
    call _sacar_largo
    mov dword[contadory],edx
;por ultimo se convierten a numeros enteros para poder operarlos
;estos resultados de numeros enteros se guardan tambien en memoria
.convertir_ascii:
    mov eax, dword[x]
    lea esi, [eax]
    mov ecx, dword[contadorx]
    call _ascii_numero
    mov dword[numerox],eax
    mov eax, dword[y]
    lea esi, [eax]
    mov ecx, dword[contadory]
    call _ascii_numero
    mov dword[numeroy],eax
    

;esto llama a la función que crea la matriz por primera vez
    jmp _inicializa_matriz

;se crea un menu con 3 mensajes
;registros utilizados:
; edx, ecx , eax, ebx

_iniciar_cargar:
    pop eax
    pop eax
    mov dword[argumento_opcion], eax
    call _sacar_largo
    mov dword[largo_opcion],edx
    pop eax
    mov dword[archivo_matriz3], eax
    mov edx, dword[archivo_matriz3]
    jmp _cargar_juego

_menu:
    mov ecx, mensaje_menu
    mov edx, largo_menu
    call DisplayText

.entrada_menu:
    mov eax, 3
    mov ebx, 2
    mov ecx, entrada_usuario
    mov edx, 2
    int 80h
;se reduce el tamaño de eax para descontar el cambio de linea al introducir el ascii
    dec eax
    cmp eax, 0;se valida en caso de que no se ingrese nada
    jz _menu
;llama a la subrutina de _ascii_numero, se encarga de pasar un ascii a entero
;se le envia un puntero en esi y un contador con el tamaño del numero introducido
    lea esi, [entrada_usuario]
    mov ecx, 1
    call _ascii_numero

    cmp ebx, 1
    je _imprimir_mensaje
    cmp ebx, 2
    je _open
    jmp _menu

_open:
    jmp _abrir_archivo_matriz_sin_cargar

;imprime el mensaje para pedir el tamaño maximo de la matriz
;registros utilizados:
; edx, ecx , eax, ebx
_imprimir_mensaje:
    mov edx,largo
    mov ecx,mensaje
    call DisplayText

;imprime el mensaje para pedir x
;registros utilizados:
; edx, ecx , eax, ebx
_imprimir_mensaje1:
    mov edx,largo1
    mov ecx,mensaje1
    call DisplayText

;Guarda el numero en ascii y entero
;registros utilizados:
; edx, ecx , eax, ebx, esi
_entrada_x:
    mov eax, 3
    mov ebx, 2
    mov ecx, x
    mov edx, largo1
    int 80h
;se reduce el tamaño de eax para descontar el cambio de linea al introducir el ascii
    dec eax
    cmp eax, 0
    jz _imprimir_mensaje1
    mov dword[contadorx], eax
;llama a la subrutina de _ascii_numero, se encarga de pasar un ascii a entero
;se le envia un puntero en esi y un contador con el tamaño del numero introducido
    lea esi, [x]
    mov ecx, dword[contadorx]
    call _ascii_numero
    cmp eax, 0
    jz _mensaje_error
    cmp eax, 16
    jge _limite_matriz
;se almacena en numerox el entero de lo introducido por el usuario
    mov dword[numerox],eax

;imprime el mensaje para pedir x
;registros utilizados:
; edx, ecx , eax, ebx
_imprimir_mensaje2:
    mov edx,largo4
    mov ecx,mensaje2
    call DisplayText

;Guarda el numero en ascii y entero
;registros utilizados:
; edx, ecx , eax, ebx, esi
_entrada_y:
    mov eax, 3
    mov ebx, 2
    mov ecx, y
    mov edx, largo5
    int 80h
;se reduce el tamaño de eax para descontar el cambio de linea al introducir el ascii
    dec eax
    cmp eax, 0
    jz _imprimir_mensaje2
    mov dword[contadory], eax

;llama a la subrutina de _ascii_numero, se encarga de pasar un ascii a entero
;se le envia un puntero en esi y un contador con el tamaño del numero introducido
    lea esi, [y]
    mov ecx, dword[contadory]
    call _ascii_numero
    cmp eax, 0
    jz _mensaje_error
    cmp eax, 16
    jge _limite_matriz
;guarda el tamaño de numeroy en entero
    mov dword[numeroy],ebx

;multiplica los numeros enteros pertenecientes a numerox y numeroy
;y el resultado es utilizado como contador para insertar espacios en blanco en la matriz
;registros utilizados:
; eax, ebx, ecx, esi
_inicializa_matriz:
    mov eax,dword[numerox]
    mov ebx, eax
    mov eax,dword[numeroy]
    imul ebx, eax ; aqui se multiplican ambos numeros
    mov dword[total_matriz],ebx; aca pasamos el largo de la matriz
    mov ecx, ebx ; se guarda en ecx para utilizarlo en un ciclo
    lea esi, [matriz] ;puntero al inicio de la matriz
;se encarga de meter un espacio en blanco en la memoria y mover el puntero
;se repite hasta llegar al final de la matriz
.meter_espacio:
    mov [esi], byte " "
    inc esi
    loop .meter_espacio
    mov eax,[matriz]
;llama a la subrutina de imprimir matriz, necesita un puntero al inicio de la matriz y el entero de y
    mov ecx, [numeroy]
    lea esi, [matriz]
    call _imprimir_matriz

;imprime un mensaje mientras el juego carga
;registros utilizados:
; edx, ecx , eax, ebx
_imprimir_mensaje3:
    mov edx,largo8
    mov ecx,mensaje3
    call DisplayText

;inserta "P" para representar el muñeco que se va a mover 
;para esto realiza varios random y los introduce en memoria
;seguidamente despliega el muñeco en la matriz por medio de esa entrada "x" y "y" randoms
;registros utilizados:
; edx, ecx , eax, ebx, esi


_inserta_persona:   
    call _random_x
    mov dword[entrada_desplegable_y],edx
    mov dword[posicionPX],edx
    call _random_y
    mov dword[entrada_desplegable_x],edx
    mov dword[posicionPY],edx
    mov eax,dword[entrada_desplegable_y]; se mete el entero de y
    mov edx,dword[entrada_desplegable_x]; aqui se mete el entero de x
    lea esi, [matriz]; puntero a inicio de matriz
    call _inserta_desplegable ;asi se llama la subrutina
	mov ecx, [numeroy] ; entero de total de y's metidas
	lea esi, [matriz]  ;puntero a inicio de matriz
    call _imprimir_matriz ;llamada a otra subrutina de impresion


    mov ecx,4 ;Cantidad de bombas

;inserta "B" para representar la bomba 
;para esto realiza varios random y los introduce en memoria
;seguidamente despliega la bomba en la matriz por medio de esa entrada "x" y "y" randoms
;registros utilizados:
; edx, ecx , eax, ebx, esi
_despliega_bomba:
    push ecx
    call _random_x
    mov dword[BombaY],edx
    call _pausa
    call _random_y
    mov dword[BombaX],edx
    call _pausa
    mov eax,dword[BombaY]; se mete el entero de y
    mov edx,dword[BombaX]; aqui se mete el entero de x
    lea esi, [matriz]; puntero a inicio de matriz
    call _inserta_bomba ;asi se llama la subrutina
	mov ecx, [numeroy] ; entero de total de y's metidas
	lea esi, [matriz]  ;puntero a inicio de matriz
    ;call _imprimir_matriz ;llamada a otra subrutina de impresion
    call _pausa
    pop ecx
    loop _despliega_bomba


;inserta "T" para representar el tesoro
;para esto realiza varios random y los introduce en memoria
;seguidamente despliega el tesoro en la matriz por medio de esa entrada "x" y "y" randoms
;registros utilizados:
; edx, ecx , eax, ebx, esi
_despliega_tesoro:
    call _random_x
    mov dword[TesoroY],edx
    call _pausa
    call _random_y
    mov dword[TesoroX],edx
    call _pausa
    mov eax,dword[TesoroY]; se mete el entero de y
    mov edx,dword[TesoroX]; aqui se mete el entero de x
    lea esi, [matriz]; puntero a inicio de matriz
    call _pausa
    call _inserta_tesoro ;asi se llama la subrutina
	mov ecx, [numeroy] ; entero de total de y's metidas
	lea esi, [matriz]  ;puntero a inicio de matriz
    call _imprimir_matriz ;llamada a otra subrutina de impresion


;imprime un mensaje pidiendo el movimiento wasd del usuario
;registros utilizados:
; edx, ecx , eax, ebx
_imprimir_mensaje4:
    mov edx,largo9
    mov ecx,mensaje4
    call DisplayText

;Guarda la letra en ascii y la convierte a un entero que queda en ebx
;registros utilizados:
; edx, ecx , eax, ebx, esi
_entrada_movimiento:
    mov eax, 3
    mov ebx, 2
    mov ecx, entrada_movimiento
    mov edx, largo10
    int 80h
    dec eax
    cmp eax, 1
    jnz _compara_movimientos.accion_nula
    mov dword[entrada_movimiento_tamanno], eax 
    
;convierte el ascii a entero por medio de la subrutina
;queda en ebx
    lea esi, [entrada_movimiento]
    mov ecx, dword[entrada_movimiento_tamanno]
    call _ascii_numero
;envia los enteros que corresponde a la posicion del personaje a la subrutina
    mov eax,dword[entrada_desplegable_y]
    mov edx,dword[entrada_desplegable_x]
    call _compara_movimientos
;llama a la subrutina de imprimir matriz, necesita un puntero al inicio de la matriz y el entero de y
    mov ecx, [numeroy] ; entero de total de y's metidas
    lea esi, [matriz]  ;puntero a inicio de matriz
    call _imprimir_matriz ;llamada a otra subrutina de impresion
;esto lo convierte en un ciclo para que el usuario se pueda seguir moviendo
    jmp _imprimir_mensaje4

;finaliza el programa
_fin:
    mov ebx,0
    mov eax,1
    int 0x80

;Subrutina que se encarga de mover al jugador por el tablero
;utiliza los registros ebx, ecx, edx , eax, esi
;para ser llamada ocupa en ebx un entero que corresponde a una letra wasd, y necesita en eax, edx los enteros de y y x respectivamente
;se encarga de validar cual letra introduce, mostrar bombas y guardar
_compara_movimientos:
    cmp ebx, 71
    jz .es_w
    cmp ebx, 49
    jz .es_a
    cmp ebx, 67
    jz .es_s
    cmp ebx, 52
    jz .es_d
    cmp ebx, 70
    jz _imprimir_bombas
    cmp ebx, 1
    jz _guardar_partida_matriz

    mov ecx, mensaje_error
    mov edx, largo_error
    call DisplayText
    ;pop ecx; saca la direccion de memoria de la pila que seguia por el call
    jmp _entrada_movimiento

;mueve a P si es a lo introducido
.es_a:

    cmp eax, 0; comprueba si el jugador está en un límite en la izquierda de la matriz
    jz .accion_nula; en eax está la posición en las columnas donde se encuentra el jugador
    
    ;guardamos eax y edx que serán modificados por _limpiador
    push eax
    push edx
    lea esi, [matriz]
    call _limpiador ;se encarga de poner un espacio en blanco en una posicion de la memoria
    ;sacamos edx y eax y restamos a eax que será y para que vaya a la izquierda
    pop edx
    pop eax
    dec eax
    ;llamada a subrutina que se encarga de validar ganar o perder
    ;le enviamos los enteros y un puntero al inicio de la matriz
    mov dword[entrada_desplegable_y],eax
    mov dword[entrada_desplegable_x],edx
    lea esi, [matriz]
    call _verifica_final
;despues de modificar los enteros de x y y y borrarlos, volvemos a insertar las nuevas coordenadas
;le enviamos los enteros y un puntero al inicio de la matriz
    mov eax,dword[entrada_desplegable_y]
    mov edx,dword[entrada_desplegable_x]
    lea esi, [matriz]
    call _inserta_desplegable
    ret
;mueve a P si es w lo introducido
;note que es_w, es_d y es_s tiene la misma funcionalidad que a pero modifican el x y y de maneras diferentes
;por lo tanto su documentacion seria la misma
;el cambio consta en incrementar o decrementar un registro diferente unicamente
.es_w:

    cmp edx, 0; comprueba si el jugador está en un límite en la fila de arriba de la matriz
    jz .accion_nula; edx tiene la posicion en las filas donde está el jugador

    push eax
    push edx
    lea esi, [matriz]
    call _limpiador
    pop edx
    pop eax
    dec edx
    mov dword[entrada_desplegable_y],eax
    mov dword[entrada_desplegable_x],edx
    lea esi, [matriz]
    call _verifica_final
    mov eax,dword[entrada_desplegable_y]
    mov edx,dword[entrada_desplegable_x]
    lea esi, [matriz]
    call _inserta_desplegable
    ret
;mueve a P si es s lo introducido
;note que es_w, es_d y es_s tiene la misma funcionalidad que a pero modifican el x y y de maneras diferentes
;por lo tanto su documentacion seria la misma
;el cambio consta en incrementar o decrementar un registro diferente unicamente
.es_s:

    mov ebx, [numeroy]; comprueba si el jugador está en un límite en la fila de abajo de la matriz
    sub ebx, 1; ebx tiene la cantidad de filas que introdució el usuario
    cmp edx, ebx; se comprueba si el jugador se encuentra en la última fila de la matiz
    jz .accion_nula

    push eax
    push edx
    lea esi, [matriz]
    call _limpiador
    pop edx
    pop eax
    inc edx
    mov dword[entrada_desplegable_y],eax
    mov dword[entrada_desplegable_x],edx
    lea esi, [matriz]
    call _verifica_final
    mov eax,dword[entrada_desplegable_y]
    mov edx,dword[entrada_desplegable_x]
    lea esi, [matriz]
    call _inserta_desplegable
    ret
;mueve a P si es d lo introducido
;note que es_w, es_d y es_s tiene la misma funcionalidad que a pero modifican el x y y de maneras diferentes
;por lo tanto su documentacion seria la misma
;el cambio consta en incrementar o decrementar un registro diferente unicamente
.es_d:
    
    mov ebx, [numerox]; comprueba si el jugador está en un límite en la columna derecha de la matriz
    sub ebx, 1; ebx tiene la cantidad de columnas que puso el jugador
    cmp eax, ebx;comprueba si el jugador se encuentra en la última columna de la matriz
    jz .accion_nula

    push eax
    push edx
    lea esi, [matriz]
    call _limpiador
    pop edx
    pop eax
    inc eax
    mov dword[entrada_desplegable_y],eax
    mov dword[entrada_desplegable_x],edx
    lea esi, [matriz]
    call _verifica_final
    mov eax,dword[entrada_desplegable_y]
    mov edx,dword[entrada_desplegable_x]
    lea esi, [matriz]
    call _inserta_desplegable
    ret

.accion_nula:
    mov ecx, [numeroy]; en caso de que el jugador esté en los límites y ya no se puede mover, se vuelve a imprimir
    lea esi, [matriz];  la matriz y no se hace nada
    call _imprimir_matriz
    jmp _imprimir_mensaje4

;subrutina que pasa de ascii a entero
; utiliza los registros:
; ebx, eax, esi
;para llamarla necesita en esi un puntero al inicio del ascii para pasar a entero
; y ocupa en ecx el tamaño de los caracteres introducidos, es decir si son uno, dos o mas letras
_ascii_numero:
    xor ebx, ebx
.siguiente_dig:
    movzx eax,byte[esi]
    inc esi
    sub al, 48
    imul ebx,10
    add ebx,eax
    loop .siguiente_dig
    mov eax,ebx
    ret

_numero_ascii: ; convierte números enteros a ascii, necesita tener en dl el número a pasar, y lo retorna ya en ascii en ecx
    or dl,30h         ;esto modifica el byte tratado a ascii
    mov ecx, edx ;se guarda el byte modificado
    ret


;subrutina que despliega texto en pantalla con llamadas al sistema
;modifica eax y ebx
; para llamarla necesita en ecx un puntero al inicio del texto y el tamaño en edx
DisplayText:
    mov eax, 4
    mov ebx, 1
    int 80h
    ret

;subrutina para introducir en matriz
;ocupa estas entradas:
;eax un numero entero que indique el y deseado
;edx un numero entero que indique el x deseado
;en esi ocupa un puntero al inicio de la matriz
; no tiene ninguna salida pero mete una "P" en memoria que seria el jugador

_inserta_desplegable:
    mov ecx,eax
    cmp eax,0 ;esto verifica el caso en el que y sea cero
    jz .verificar_y
;si no fuera 0 mueve esi hasta que lo sea
.mover_x:
    inc esi
    loop .mover_x
.verificar_y:
    mov ecx, edx
    cmp edx,0
    jz .meter_desplegable ;verifica si x es 0
    mov eax,dword[numerox]
;en caso de no serlo mueve esi hasta que lo sea
.mover_y:
    add esi,eax
    loop .mover_y
;al llegar a la ultima posicion introduce P en ese byte
.meter_desplegable:
    mov [esi], byte "P"
    ret

;subrutina para verificar si usuario ya ganó o perdió
;ocupa estas entradas:
;eax un numero entero que indique el y deseado
;edx un numero entero que indique el x deseado
;en esi ocupa un puntero al inicio de la matriz
_verifica_final:
    mov ecx,eax
    cmp eax,0
    jz .verificar_y_final
.mover_x_final:
    inc esi
    loop .mover_x_final
.verificar_y_final:
    mov ecx, edx
    cmp edx,0
    jz .revisa_final
    mov eax,dword[numerox]
.mover_y_final:
    add esi,eax
    loop .mover_y_final
;todo lo anterior funciona igual que _inserta_desplegable para llegar a una posicion en esi
;esto guarda lo apuntado por esi en memoria
.revisa_final:
    mov ecx,dword[esi]
    mov dword[letra_revisada],ecx
    lea esi, [letra_revisada]
    mov ecx, 1
    call _ascii_numero ;convierte nuestro espacio en memoria en un entero para hacer validaciones

;valida si ebx es igual a alguno de los casos y se encarga de imprimir el mensaje deseado
.llama_gana_pierde:
    cmp ebx, 18
    jz .imprimir_mensaje_perdedor
    cmp ebx, 36
    jz .imprimir_mensaje_ganador
    jmp .continuar_normal
;imprime un mensaje de ganador en pantalla y finaliza el programa
.imprimir_mensaje_ganador:
    mov edx,largo_ganador
    mov ecx,mensaje_ganador
    call DisplayText
    jmp _fin
;imprime un mensaje de perdedor en pantalla y finaliza el programa
.imprimir_mensaje_perdedor:
    mov edx,largo_perdedor
    mov ecx,mensaje_perdedor
    call DisplayText
    jmp _fin
;si no se cumple ninguno de los casos simplemente regresa
.continuar_normal:
    ret

_inserta_bomba:
    mov ecx,eax
    cmp eax,0
    jz .verificar_bomba_y
.mover_bomba_x:
    inc esi
    loop .mover_bomba_x
.verificar_bomba_y:
    mov ecx, edx
    cmp edx,0
    jz .meter_desplegable_bomba
    mov eax,dword[numerox]
.mover_bomba_y:
    add esi,eax
    loop .mover_bomba_y
.meter_desplegable_bomba:
    mov [esi], byte "B"
    ret

;subrutina para introducir en matriz
;ocupa estas entradas:
;eax un numero entero que indique el y deseado
;edx un numero entero que indique el x deseado
;en esi ocupa un puntero al inicio de la matriz
; no tiene ninguna salida pero mete una "T" en memoria que seria el tesoro
_inserta_tesoro:
    mov ecx,eax
    cmp eax,0
    jz .verificar_tesoro_y
.mover_tesoro_x:
    inc esi
    loop .mover_tesoro_x
.verificar_tesoro_y:
    mov ecx, edx
    cmp edx,0
    jz .meter_desplegable_tesoro
    mov eax,dword[numerox]
.mover_tesoro_y:
    add esi,eax
    loop .mover_tesoro_y
.meter_desplegable_tesoro:
    mov [esi], byte "T"
    ret

;necesita tener en eax el argumento
_sacar_largo:
    mov ecx, 0

.sacar_largo_aux:
    cmp byte[eax], 0; compara el primer caracter con nulo
    jz .finaliza; si es cero ya finaliza
    inc eax; si no es nulo, incrementa el puntero al siguiente caracter
    inc ecx; incrementa el largo del argumento
    jmp .sacar_largo_aux; continua con el siguiente caracter
;finaliza sacar_largo
.finaliza:
    mov edx, ecx
    ret


;subrutina para "limpiar" un lugar en matriz
;ocupa estas entradas:
;eax un numero entero que indique el y deseado
;edx un numero entero que indique el x deseado
;en esi ocupa un puntero al inicio de la matriz
; no tiene ninguna salida pero mete un " " en memoria que seria el lugar limpiado
;su funcionalidad es igual a _inserta_desplegable pero introduce un espacio en lugar de una P
_limpiador:
    mov ecx,eax
    cmp eax,0
    jz .verificar_y
.mover_x:
    inc esi
    loop .mover_x
.verificar_y:
    mov ecx, edx
    cmp edx,0
    jz .meter_desplegable
    mov eax,dword[numerox]
.mover_y:
    add esi,eax
    loop .mover_y
.meter_desplegable:
    mov [esi], byte " " ;esta linea es la unica diferencia
    ret

;subrutina para imprimir la matriz con las bombas ocultas
; requerimentos:
; el registro esi debe estar apuntando al inicio de la matriz
; ecx debe tener la cantidad de filas introducidas por el usuario (numeroy)
_imprimir_matriz:
    push ecx; se mete a la pila la cantidad de filas
    mov ecx, linea_horizontal
    mov edx, [numerox]
    add edx, 1
    imul edx, 4
    sub edx, 2
    call DisplayText; se imprime una linea horizontal
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText; se imprime un salto de linea
    mov ecx, [numerox]; movemos a ecx la cantidad de columnas
    call .imprimir_contenido; empezamos a imprimir de fila en fila la matriz
    pop ecx
    loop _imprimir_matriz; hacemos loop hasta que se impriman todas las filas
    mov ecx, linea_horizontal
    mov edx, [numerox]
    add edx, 1
    imul edx, 4
    sub edx, 2
    call DisplayText; imprimimos una última línea horizontal
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText; se imprime un salto de linea
    ret
    
.imprimir_contenido:
    push ecx; guardamos en la pila la cantidad de columnas
    mov ecx, linea_vertical
    mov edx, largo_vertical
    call DisplayText; imprimimos una linea vertical
    push esi
    mov ecx, 1
    call _ascii_numero
    pop esi
    cmp ebx, 18; comprobamos que lo que esta en la matriz no sea una bomba
    jz .imprimir_espacio; si lo es, se imprime un espacio
    mov ecx, esi; si no, movemos a ecx lo que se esté apuntando de la matriz
    mov edx, 1
    call DisplayText; se imprime un caracter de la matriz
    inc esi; se incrementa el esi para pasar a la siguiente letra
    pop ecx
    loop .imprimir_contenido; hacemos un ciclo hasta que se acabe la fila
    mov ecx, linea_vertical
    mov edx, largo_vertical
    call DisplayText; imprimimos una última linea vertical
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText; imprimimos un salto de linea
    ret

; esta función solo imprime un espacio y continúa con el ciclo normal de imprimir matriz
.imprimir_espacio:
    mov ecx, espacio
    mov edx, 1
    call DisplayText
    inc esi
    pop ecx
    loop .imprimir_contenido
    mov ecx, linea_vertical
    mov edx, largo_vertical
    call DisplayText
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText
    ret

; esta función imprime la matriz pero muestra al usuario donde están las bombas
; esta función es igual a la de imprimir, pero no es necesario hacer la validación de si encuentra una B
_imprimir_bombas:
    mov ecx, [numeroy]
    lea esi, [matriz]
.imprimir_bombas_aux:
    push ecx
    mov ecx, linea_horizontal
    mov edx, [numerox]
    add edx, 1
    imul edx, 4
    sub edx, 2
    call DisplayText
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText
    mov ecx, [numerox]
    call .imprimir_contenido2
    pop ecx
    loop .imprimir_bombas_aux
    mov ecx, linea_horizontal
    mov edx, [numerox]
    add edx, 1
    imul edx, 4
    sub edx, 2
    call DisplayText
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText
    ret
    
.imprimir_contenido2:
    push ecx
    mov ecx, linea_vertical
    mov edx, largo_vertical
    call DisplayText
    mov ecx, esi
    mov edx, 1
    call DisplayText
    inc esi
    pop ecx
    loop .imprimir_contenido2
    mov ecx, linea_vertical
    mov edx, largo_vertical
    call DisplayText
    mov ecx, salto_linea
    mov edx, 1
    call DisplayText
    ret


;Random por medio de la llamda sys_time
;Realiza randoms y a la vez pausas para que sys_time pueda cambiar y asi obtener otro valor random
;Una vez realizado el random por medio de un modulo se baja el rango que tira este y después se guarda en memoria
;Realiza un random para"y" y otra para "x"
;registros utilizados;
;eax,edx,ecx
_random_x:
    call _pausa
    nop
    call _srand
    call _rand_x
    
_rand_x:
    mov eax, [numero]      ;mover a eax para realizar mul
    mov ebx, 1103515245   ; factor de multiplicacion
    mul ebx               ; eax = eax * ebx
    add eax, 12345        ; incrementar
    mov dword [numero], eax ; update numero value
    mov ebx,dword[numeroy]               ; modulo del aleatorio
    xor edx, edx          ; evitar cualquier numero punto flotante
    div ebx               ; edx tiene el numero aleatorio
    ret

_random_y:
    call _pausa
    nop
    call _srand
    call _rand_y
    
_rand_y:
    mov eax, [numero]      ;mover a eax para realizar mul
    mov ebx, 1103515245   ; factor de multiplicacion
    mul ebx               ; eax = eax * ebx
    add eax, 12345        ; incrementar
    mov dword [numero], eax ; update numero value
    mov ebx,dword[numerox]               ; modulo del aleatorio
    xor edx, edx          ; evitar cualquier numero punto flotante
    div ebx               ; edx tiene el numero aleatorio
    ret                   
_srand:
    mov eax, 0x0d         ; sys_time
    mov ebx, 0x0          ; NULL
    int 0x80              
    mov dword [numero], eax 
    ret 


_pausa:
    mov dword [tv_sec], 1
    mov dword [tv_usec],0
    mov eax, 162
    mov ebx, tiempos
    mov ecx, 0
    int 80h
    ret

_mensaje_error:; imprime un mensaje de error por si se introducen matrices con 0
    mov ecx, mensaje_error2
    mov edx, largo_error2
    call DisplayText
    jmp _imprimir_mensaje1

_limite_matriz:; imprime un mensaje de error por si se introducen matrices mayores a 15
    mov ecx, limite_matriz
    mov edx, largo_limite
    call DisplayText
    jmp _imprimir_mensaje1

;guarda la matriz llamando las funciones de manejar archivos
;registros usados
;eax,ebx,ecx,edx
_guardar_partida_matriz:
    
    lea esi, [x] ;comparación para saber si la partida actual se inició por menú principal o por argumentos
    mov ecx, dword[contadorx]
    call _ascii_numero
    cmp eax, 16
    jge _guardar_partida_matriz_arg
    
    call _abrir_archivo_matriz
    call _escribe_archivo_numerox
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz

    call _abrir_archivo_matriz
    call _escribe_archivo_numeroy
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz

    call _abrir_archivo_matriz
    call _escribe_archivo_matriz
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    call _abrir_archivo_matriz
    call _escribe_posicionX
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    call _abrir_archivo_matriz
    call _escribe_posicionY
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    ret

_guardar_partida_matriz_arg:
    
    call _abrir_archivo_matriz
    call _escribe_archivo_numerox_arg
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz

    call _abrir_archivo_matriz
    call _escribe_archivo_numeroy_arg
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz

    call _abrir_archivo_matriz
    call _escribe_archivo_matriz
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    call _abrir_archivo_matriz
    call _escribe_posicionX
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    call _abrir_archivo_matriz
    call _escribe_posicionY
    call _escribe_archivo_separador
    call _cerrar_archivo_matriz
    
    ret
;carga el juego que se guardo previamente 
;para esto anteriormente en guardar partida separamos los datos con "="para en esta función leerlos y utilizarlos.
;registros utilizados
;eax,ecx,edx,ebx,esi,edi
_cargar_juego:

    call _abrir_archivo_matriz
.sin_cargar:
    call _leer_archivo_matriz
    call _cerrar_archivo_matriz

    lea esi,[matriz_cargada]
    mov edx, 0
    mov ecx, esi

.cargar_aux_x:
    mov ecx, 1
    call _ascii_numero
    cmp ebx, 13
    je .fucn
    add edx, 1
    jmp .cargar_aux_x
.fucn:
    lea esi, [matriz_cargada]
    mov ecx, edx
    call _ascii_numero
    mov dword[numerox], ebx
    inc esi
    mov edx, 0
    push esi
.cargar_aux_y:
    mov ecx, 1
    call _ascii_numero
    cmp ebx, 13
    je .fucn2

    add edx, 1
    jmp .cargar_aux_y
.fucn2:
    pop esi
    mov ecx, edx
    call _ascii_numero
    mov dword[numeroy], ebx
    inc esi
    lea edi, [matriz]
.cargar_matriz:
    mov eax, esi;metemos a eax la matriz archivo
    lea esi, [edi];movemos a esi la matriz a cargar
    push esi;metemos la matriz a cargar a la pila
    lea esi, [eax];movemos a esi una letra de la matriz archivo
    mov edx, eax;movemos a edx matriz archivo
    mov ecx, 1
    call _ascii_numero;comprobamos cada caracter de la matriz archivo
    cmp ebx, 13
    je .terminar
    cmp ebx,18
    je .inserta_B
    cmp ebx,32
    je .inserta_P
    cmp ebx,36
    je .inserta_T
    pop esi;saca la matriz a cargar a esi
    mov [esi],byte " ";movemos a esa parte la letra de la matriz archivo
    inc esi;matriz a cargar; se incrementa matriz a cargar
    lea edi, [esi]
    inc edx;matriz archivo; se incrementa matriz
    lea esi, [edx];movemos a esi la matriz archivo, apuntando al siguiente caracter
    jmp .cargar_matriz


.terminar:
    mov ecx, [numeroy]
    lea esi, [matriz]
    call _imprimir_matriz
    jmp _imprimir_mensaje4
.inserta_B:
    pop esi;saca la matriz a cargar a esi
    mov [esi],byte "B";movemos a esa parte la letra de la matriz archivo
    inc esi;matriz a cargar; se incrementa matriz a cargar
    lea edi, [esi]
    inc edx;matriz archivo; se incrementa matriz
    lea esi, [edx]
    jmp .cargar_matriz
.inserta_P:
    pop esi;saca la matriz a cargar a esi
    mov [esi],byte "P";movemos a esa parte la letra de la matriz archivo
    inc esi;matriz a cargar; se incrementa matriz a cargar
    lea edi, [esi]
    inc edx;matriz archivo; se incrementa matriz
    lea esi, [edx]
    jmp .cargar_matriz
.inserta_T:
    pop esi;saca la matriz a cargar a esi
    mov [esi],byte "T";movemos a esa parte la letra de la matriz archivo
    inc esi;matriz a cargar; se incrementa matriz a cargar
    lea edi, [esi]
    inc edx;matriz archivo; se incrementa matriz
    lea esi, [edx]
    jmp .cargar_matriz

;se abre el archivo por medio de sys_open que es una llamada al sistema.
;ecx son las banderas, en este caso de la bandera de creat,append,read and write todo en octal.
;el creat crea el archivo si no está creado, el append se dirige al final del archivo(si hay un nombre una linea abajo del nombre), y da una bandera para leer(read) y escribir(write).
;ebx donde se creara el archivo, si esta creado solo abre el archivo.
;eax llamamos a sys_open por medio de 8.
;edx son los permisos para el archivo, en este caso se da permiso de leer, escribir y ejecutar.
;Después de abrirse con éxito, en el registro eax se dejará el descriptor del archivo creado o abierto por lo que se guarda en id.

_abrir_archivo_matriz_cargada:
      mov eax, [edx]
      mov ecx,2102o
      push edx
      mov edx,00700o
      pop ebx
      mov eax,5
      int 80h
      mov dword[id1],eax
      ret
_abrir_archivo_matriz_sin_cargar:
      mov eax, dword[archivo_matriz2]
      mov ecx,2102o
      mov edx,00700o
      mov ebx, archivo_matriz2
      mov eax,5
      int 80h
      mov dword[id1],eax
      jmp _cargar_juego.sin_cargar
_abrir_archivo_matriz:
      mov eax, dword[archivo_matriz2]
      mov ecx,2102o
      mov edx,00700o
      mov ebx, archivo_matriz2
      mov eax,5
      int 80h
      mov dword[id1],eax
      ret

;se lee el archivo por medio de sys_read que es una llamada al sistema.
;ecx se guarda lo que contiene del archivo.
;ebx se pone el descriptor de archivo.
;eax llamamos a sys_read por medio de 3.
;edx se pone la cantidad a leer del archivo.
;Después de leerse con éxito, en el registro eax se dejará el descriptor del archivo leído por lo que se guarda en id.

_leer_archivo_matriz:

      mov edx,250
      mov ecx,matriz_cargada
      mov ebx,dword[id1]
      mov eax,3
      int 80h
      mov ecx, dword[matriz_cargada]
      
      ret
;se escribe en el archivo por medio de sys_write que es una llamada al sistema.
;ecx se envía el mensaje a escribir (en esta caso entrada es el nombre que el usuario ingresó).
;edx se envía el largo del mensaje a escribir.
;ebx descriptor del archivo en donde se escribirá.
;eax llamamos a sys_write por medio de 4.

_escribe_archivo_matriz:
        mov edx,dword[total_matriz]
        mov ecx,matriz
        mov ebx,dword[id1]
        mov eax, 4
        int 80h
        
        ret

_escribe_archivo_numerox_arg:
        mov eax, dword[x]
        mov edx,dword[contadorx]
        mov ecx,eax
        mov ebx,dword[id1]
        mov eax, 4
        int 80h
        
        ret

_escribe_archivo_numeroy_arg:
        mov eax, dword[y]
        mov edx,dword[contadory]
        mov ecx,eax
        mov ebx,dword[id1]
        mov eax, 4
        int 80h
        
        ret


_escribe_archivo_numerox:
        mov edx,dword[contadory]
        mov ecx,y
        mov ebx,dword[id1]
        mov eax, 4
        int 80h
        
        ret


_escribe_archivo_numeroy:
        mov edx,dword[contadory]
        mov ecx,y
        mov ebx,dword[id1]
        mov eax, 4
        int 80h
        
        ret

_escribe_archivo_separador:
        mov edx,1
        mov ecx,separador
        mov ebx,dword[id1]
        mov eax, 4
        int 80h    
        ret
        
_escribe_posicionX:
        mov eax, [posicionPX]
        cmp eax, 10
        jge _dos_digitos
        mov ecx, 1
        jmp _convierte_num_asciiX

_escribe_posicionY:
        mov eax, [posicionPY]
        cmp eax, 10
        jge _dos_digitos
        mov ecx, 1
        jmp _convierte_num_asciiY



_escribe_archivo_posicionXY:
        mov edx, eax
        mov eax,4 
        mov ecx, esi
        mov ebx, dword[id1]
        int 80h
        ret
        
_dos_digitos:
    xor ebx, ebx
    lea esi, [ebx]
    push esi
    mov ecx, 10
    xor edx, edx
    div ecx
    push edx
    xor edx, edx
    mov edx, eax
    call _numero_ascii
    lea esi, [esp + 4]
    mov [esi], ecx
    inc esi
    pop edx
    call _numero_ascii
    mov [esi], ecx
    lea esi, [esp]
    pop ebx
    mov eax, 2
    jmp _escribe_archivo_posicionXY
    

_convierte_num_asciiX:
    lea esi, [posicionPX]
    push esi
    mov eax, ecx
    jmp _convierte_num_ascii_aux
        
_convierte_num_asciiY:
    lea esi, [posicionPY]
    push esi
    mov eax, ecx
    jmp _convierte_num_ascii_aux
    
_convierte_num_ascii_aux:
    push ecx
    mov dl, [esi]
    call _numero_ascii
    mov [esi], ecx
    pop ecx
    loop _convierte_num_ascii_aux
    pop esi
    jmp _escribe_archivo_posicionXY
;se cierra el archivo por medio de sys_close que es una llamada al sistema.
;ebx descriptor de archivo a cerrar.
;eax, llamamos a sys_close por medio de 6.

_cerrar_archivo_matriz:

      mov ebx,dword[id1]
      mov eax,6
      int 80h
      ret
